import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TaskComponent } from './task/task.component';
import { AnswerComponent } from './answer/answer.component';
import { PromptComponent } from './prompt/prompt.component';
import { UserAnswerComponent } from './user-answer/user-answer.component';
import { TaskServices } from './shered/task.services';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    TaskComponent,
    AnswerComponent,
    PromptComponent,
    UserAnswerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [TaskServices],
  bootstrap: [AppComponent]
})
export class AppModule { }
