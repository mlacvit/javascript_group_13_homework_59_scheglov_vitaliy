import { Component, ElementRef, ViewChild } from '@angular/core';
import { TaskServices } from '../shered/task.services';

@Component({
  selector: 'app-user-answer',
  templateUrl: './user-answer.component.html',
  styleUrls: ['./user-answer.component.css']
})
export class UserAnswerComponent {
  @ViewChild('userInput') userInput!: ElementRef;

  constructor(private taskServices: TaskServices) { }

  addValue() {
    this.taskServices.userAnswer = this.userInput.nativeElement.value;
  }



}
