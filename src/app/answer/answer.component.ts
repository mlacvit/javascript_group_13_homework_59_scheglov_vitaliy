import { Component } from '@angular/core';
import { TaskServices } from '../shered/task.services';

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.css']
})
export class AnswerComponent  {

  constructor(public taskServices: TaskServices) { }

  addToAnswer(){
    let color = '';
    if (this.taskServices.userAnswer === this.taskServices.answer) {
      color = 'win';
      this.taskServices.result++;
    }else {
      color = 'lose'
    }
    return color;
  }
}
