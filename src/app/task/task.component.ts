import { Component } from '@angular/core';
import { TaskServices } from '../shered/task.services';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent {
  promptTask = '';
  btn = 'none';
  task = '';
  winLose = 'none';

  constructor(private taskServices: TaskServices) { }

  addToQuestion() {
    if (this.taskServices.onQuestion() === 'В каком городе находится Саграда Фамилия?'){
      this.promptTask = 'Там еще много русских туристов';
      this.task = 'В каком городе находится Саграда Фамилия?';
      this.taskServices.answer = 'Барселона';

    }if (this.taskServices.onQuestion() === 'В каком городе музей Прадо?'){
      this.promptTask = 'В центре Испании';
      this.task = 'В каком городе музей Прадо?';
      this.taskServices.answer = 'Мадрид';

    }if (this.taskServices.onQuestion() === 'Испанский остров?'){
      this.promptTask = 'Там еще все тусят';
      this.task = 'Испанский остров?';
      this.taskServices.answer = 'Ибица';

    }if (this.taskServices.onQuestion() === 'Древняя столица Испании?'){
       this.promptTask = 'Есть такой город в США';
      this.task = 'Древняя столица Испании?';
      this.taskServices.answer = 'Толедо';

    }if (this.taskServices.onQuestion() === 'Известный испанский художник?'){
      this.promptTask = 'Маска из сериала Бумажный дом';
      this.task = 'Известный испанский художник?';
      this.taskServices.answer = 'Дали';

    }if (this.taskServices.onQuestion() === 'Любимый танец испанцев?'){
      this.promptTask = 'Его еще называют страстным танцем';
      this.task = 'Любимый танец испанцев?';
      this.taskServices.answer = 'Фламенко';

    }if (this.taskServices.onQuestion() === 'Популярный архитектор Испании?'){
      this.promptTask = 'Много его творчества можно встретить в Барселоне';
      this.taskServices.answer = 'Гауди';

    }if (this.taskServices.onQuestion() === 'Назовите короля Испании?'){
      this.promptTask = 'Тезка Киркорова';
      this.task = 'Назовите короля Испании?';
      this.taskServices.answer = 'Филипп';

    }if (this.taskServices.onQuestion() === 'Бой с быком?'){
      this.promptTask = 'Красной тряпкой машет тореадор';
      this.task = 'Бой с быком?';
      this.taskServices.answer = 'Коррида';
    }
  }

  openPrompt(e: Event) {
    e.preventDefault()
     return this.btn = 'block';
  }

  closePrompt(e: Event) {
    e.preventDefault()
    return this.btn = 'none';
  }

  addResult(){
    return this.taskServices.result;
  }

}
